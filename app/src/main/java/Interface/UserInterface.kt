package Interface

import Data.User
import retrofit2.Call
import retrofit2.http.GET

interface UserInterface {
    @GET("posts/1")
    suspend fun getPost(): User

    @GET("posts")
    suspend fun getPosts(): Call<List<User>>
}