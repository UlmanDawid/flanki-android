import Interface.UserInterface
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object UserObj {
    val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("http://localhost:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(UserInterface::class.java)
    }
}